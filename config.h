//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Command*/		/*Update Interval*/	/*Update Signal*/
        //{"help",        0,        15},
	{"cpu",		10,	13},
	{"memory",	  10,       14},
 	{"volume",        1,        10},
	// {"battery | tr \'\n\' \' \'",	5,	3},
	// {"bright",        1,        10},
        {"clock",        60,        1},
        {"internet",        5,        4},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '|';
